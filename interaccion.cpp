#include "interaccion.h"
#include "hombre.h"
#include "caja.h"
#include "hexaedro.h"
#include "pentaedro.h"
#include <cmath>
#include "ETSIDI.h"
#include "bonus.h"


interaccion::interaccion(void)
{
}


interaccion::~interaccion(void)
{
}



bool interaccion:: rebote(esfera &e, pared p)
{
	vector2d dir;
	float dif=p.distancia(e.posicion,&dir)-e.radio;
	if(dif<=0.0f)
	{
		vector2d v_inicial=e.velocidad;
		e.velocidad=v_inicial-dir*2.0*(v_inicial*dir);
		e.posicion=e.posicion-dir*dif;
		
		return true;
		
		
	}
	return false;
}
void interaccion::rebote(esfera &e, caja c)
{
	float xmax=c.suelo.limite2.x;
	float xmin=c.suelo.limite1.x;
	float ymax=c.suelo.limite2.y-e.radio;
	float ymin=c.suelo.limite1.y+e.radio;
	if(e.posicion.x>xmax)
	{
		e.posicion.x=xmax;
	}
	if(e.posicion.x<xmin)
	{
		e.posicion.x=xmin;
	}

	interaccion::rebote(e,c.suelo);
	if(interaccion::rebote(e,c.techo))
		ETSIDI::play("sonidos/Golpe.mp3");
	if(interaccion::rebote(e,c.pared_dcha))
		ETSIDI::play("sonidos/Golpe.mp3");
	if(interaccion::rebote(e,c.pared_izq))
		ETSIDI::play("sonidos/Golpe.mp3");
}
bool interaccion::rebote(esfera &e, hexaedro &h)
{


	if(interaccion::rebote(e,h.suelo))
	{
		ETSIDI::play("sonidos/golpehexaedro.mp3");
		return true;
		
	}

	if(interaccion::rebote(e,h.techo))
	{
		ETSIDI::play("sonidos/golpehexaedro.mp3");
		return true;
		
	}
	if(interaccion::rebote(e,h.pared_dcha))
	{
		ETSIDI::play("sonidos/golpehexaedro.mp3");
		return true;
		
	}
	if(interaccion::rebote(e,h.pared_izq))
	{
		ETSIDI::play("sonidos/golpehexaedro.mp3");
		return true;
		
	}
	//if(interaccion::rebote(e,h.tope))
	//if(interaccion::rebote(e,h.fondo))
	return false;
}

bool interaccion::rebote(esfera &e, pentaedro &p)
{

	if(interaccion::rebote(e,p.suelo))
	{
		ETSIDI::play("sonidos/golpepentaedro.mp3");
		return true;
		
	}
	if(interaccion::rebote(e,p.delantera))
	{
		ETSIDI::play("sonidos/golpepentaedro.mp3");
		return true;
		
	}
	if(interaccion::rebote(e,p.trasera))
	{
		ETSIDI::play("sonidos/golpepentaedro.mp3");
		return true;
		
	}
	if(interaccion::rebote(e,p.pared_dcha))
	{
		ETSIDI::play("sonidos/golpepentaedro.mp3");
		return true;
		
	}
	if(interaccion::rebote(e,p.pared_izq))
	{
		ETSIDI::play("sonidos/golpepentaedro.mp3");
		return true;
		
	}
	return false;
	
}



bool interaccion::rebote(esfera &esfera1, esfera &esfera2)
{

	vector2d dif=esfera2.posicion-esfera1.posicion;
	float d=dif.modulo();
	float dentro=d-(esfera1.radio+esfera2.radio);



	if(dentro<0.0f)
	{
		
		float v1=esfera1.velocidad.modulo();
		float ang1=esfera1.velocidad.argumento();

	
		float v2=esfera2.velocidad.modulo();
		float ang2=esfera2.velocidad.argumento();
	
		
		float angd=dif.argumento();	
		
	
		vector2d desp(dentro/2*cos(angd),dentro/2*sin(angd));	
		esfera1.posicion=esfera1.posicion+desp;
		esfera2.posicion=esfera2.posicion-desp;

		angd=angd-3.14159f/2;
	
		
		float th1=ang1-angd;	
		float th2=ang2-angd;
		
	
		float u1x=v1*cos(th1);
		float u1y=v1*sin(th1);
		float u2x=v2*cos(th2);
		float u2y=v2*sin(th2);	
	
		
		float v1x=u1x;
		float v2x=u2x;

		
		float m1=esfera1.radio;
		float m2=esfera2.radio;
		float py=m1*u1y+m2*u2y;
		float ey=m1*u1y*u1y+m2*u2y*u2y;
	
		
		float a=m2*m2+m1*m2;
		float b=-2*py*m2;
		float c=py*py-m1*ey;	
		float disc=b*b-4*a*c;
		if(disc<0)disc=0;

		
		float v2y=(-b+sqrt(disc))/(2*a);
		float v1y=(py-m2*v2y)/m1;
		
		
		float fi1,fi2;
		//float modv1,modv2;

		//modv1=sqrt(v1x*v1x+v1y*v1y);
		//modv2=sqrt(v2x*v2x+v2y*v2y);
		fi1=angd+atan2(v1y,v1x);
		fi2=angd+atan2(v2y,v2x);
		
		/*
		esfera1.velocidad.x=modv1*cos(fi1);
		esfera1.velocidad.y=modv1*sin(fi1);
		esfera2.velocidad.x=modv2*cos(fi2);
		esfera2.velocidad.y=modv2*sin(fi2);	
		*/
		
		esfera1.velocidad.x=v1*cos(fi1);
		esfera1.velocidad.y=v1*sin(fi1);
		esfera2.velocidad.x=v2*cos(fi2);
		esfera2.velocidad.y=v2*sin(fi2);	
		
		return true;
	}
	return false;
}


bool interaccion::colision(esfera e, hombre h)
{
	vector2d pos=h.posicion;
	pos.y+=h.altura/2.0f;
	float distancia=(e.posicion-h.posicion).modulo();
	if(distancia<=(h.altura+e.radio))
		return true;
	return false;
}

bool interaccion::bons(esfera &e, bonus &c)
{
	if(interaccion::rebote(e,c.suelo))
	{
		return true;
		
	}

	if(interaccion::rebote(e,c.techo))
	{
		return true;
		
	}
	if(interaccion::rebote(e,c.pared_dcha))
	{
		return true;
		
	}
	if(interaccion::rebote(e,c.pared_izq))
	{
		return true;
	}
	//if(interaccion::rebote(e,h.tope))
	//if(interaccion::rebote(e,h.fondo))
	return false;
}