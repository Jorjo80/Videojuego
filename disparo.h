#include "vector2d.h"
#include "hombre.h"

#pragma once

class disparo
{
	float radio;
	vector2d origen;
	vector2d posicion;
	hombre h;

public:
	disparo();
	~disparo();
	
	void Dibuja(void);
	void Mueve();
	void Reset(void);
	void SetPos();
	friend class mundo;
};