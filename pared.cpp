#include "pared.h"
#include <stdlib.h>
#include "glut.h"
#include "ETSIDI.h"


pared::pared(void)
{
}


pared::~pared(void)
{
}

void pared::SetColor( unsigned char r,unsigned char v, unsigned char a)
{
	rojo=r;
	verde=v;
	azul=a;
}


void pared::Dibuja(int id)
{
	switch(id)
	{
	case 0:
		{
			if(limite1.z==limite2.z)
			{
				glDisable(GL_LIGHTING);
				glColor3ub(rojo,verde,azul);
				glBegin(GL_POLYGON);
					glVertex3d(limite1.x,limite1.y,limite1.z);
					glVertex3d(limite2.x,limite1.y,limite1.z);
					glVertex3d(limite2.x,limite2.y,limite2.z);
					glVertex3d(limite1.x,limite2.y,limite2.z);
				glEnd();
				glEnable(GL_LIGHTING);
			}
			else
			{
				glDisable(GL_LIGHTING);
				glColor3ub(rojo,verde,azul);
				glBegin(GL_POLYGON);
					glVertex3d(limite1.x,limite1.y,limite1.z);
					glVertex3d(limite2.x,limite2.y,limite1.z);
					glVertex3d(limite2.x,limite2.y,limite2.z);
					glVertex3d(limite1.x,limite1.y,limite2.z);
				glEnd();
				glEnable(GL_LIGHTING);
			}
			break;
		}
		case 1:
			{
				if(limite1.z==limite2.z)
				{
					glDisable(GL_LIGHTING);
					glColor3ub(rojo,verde,azul);
					glBegin(GL_POLYGON);
					glVertex3d(limite1.x,limite1.y,limite2.z);
					glVertex3d(limite2.x,limite1.y,limite2.z);
					glVertex3d(limite2.x,limite2.y,limite2.z);
					glEnd();
					glEnable(GL_LIGHTING);
				}
				else
				{
					glDisable(GL_LIGHTING);
					glColor3ub(rojo,verde,azul);
					glBegin(GL_POLYGON);
					glVertex3d(limite1.x,limite1.y,limite1.z);
					glVertex3d(limite2.x,limite2.y,limite1.z);
					glVertex3d(limite2.x,limite2.y,limite2.z);
					glEnd();
					glEnable(GL_LIGHTING);
				}
				break;
			}
		
		}
}




//Calculo de distancia de una pared a un punto, adicionalmente
//se modifica el valor de un vector direccion opcional que contendr�
//el vector unitario saliente que indica la direccion de la
//recta m�s corta entre el punto y la pared.
float pared::distancia(vector2d punto, vector2d *direccion)
{
	vector2d u=(punto-limite1);
	vector2d v=(limite2-limite1).Unitario();
	float longitud=(limite1-limite2).modulo();

	vector2d dir;
	float valor=u*v;
	float distancia=0;

	if(valor<0)
		dir=u;
	else if(valor>longitud)
			dir=(punto-limite2);
	else
			dir=u-v*valor;

	distancia=dir.modulo();
	if(direccion!=0) //si nos dan un vector�
		*direccion=dir.Unitario();

	return distancia;
}

void pared::SetPos( float x1, float y1,float z1, float x2, float y2, float z2)
{
	limite1.x=x1;
	limite1.y=y1;
	limite1.z=z1;
	limite2.x=x2;
	limite2.y=y2;
	limite2.z=z2;
}
