#include "mundo.h"
#pragma once

class CoordinadorJuego
{
public:
	CoordinadorJuego(void);
	virtual ~CoordinadorJuego(void);
	void teclaEspecial(unsigned char key);
	void tecla(unsigned char key);
	void mueve();
	void dibuja();
	mundo mundo3;
	enum Estado{Inicio, Juego, Personaje, GAMEOVER,GOODBYE,INSTRUCCIONES};
	Estado estado;
protected:
	int tipo_personaje;
	
	
};

