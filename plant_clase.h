#pragma once

#include "caja.h"
#define MAX_FIG 100

template<class T>

class Base_fig
{
	int numero;
	public:

	bool figuras_fin;
	T *lista[MAX_FIG];

	Base_fig(void)
	{
		numero=0;
		figuras_fin=false;
		for(int i=0;i<MAX_FIG;i++){
			lista[i]=0;
		}
	}
	~ Base_fig(void){}

	void destruircontenido()
	{
		for(int i=numero-1;i>=0;i--)
			delete lista[i];
		numero=0;
	}

	bool agregar (T *t)
	{
		if(numero<MAX_FIG)	
			lista[numero++]=t;
		else
			return false;
		return true;
	}
	void dibuja()
	{
		for(int i=0;i<numero;i++)
			lista[i]->Dibuja();
	}


	void mueve()
	{
		vector2d w;
		int  v;
		for (int i=numero-1;i>-1;i--)
		{
			w.x=(*lista[i]).posicion.x;
			w.y=(*lista[i]).posicion.y;
			v=(*lista[i]).vida;

			eliminar(i);
			w.y=w.y-2;
			if(w.y>2)
				agregar(new T(w.x,w.y,v));
			else
				figuras_fin=true;
		}

	
	}	
		
	void eliminar(int index)
	{
		if((index<0)||(index>=numero))
			return;
		delete lista[index];
		numero--;
		for(int i=index;i<numero;i++)
			lista[i]=lista[i+1];
	}
	void eliminar()
	{
		for(int i=numero-1;i>=0;i--)
		{
			if( (*lista[i]).vida<1)
			{
				eliminar(i);
			}
		}


	}

	T *operator[](int i)
	{
		
		if(i>numero)
			i=numero-1;
		if(i<0)
			i=0;
		return lista[i];
	}
	
	int getnumerohex(){return numero;}

	
	
	friend class Lista_Esferas;
	friend class mundo;
	friend class CoordinadorJuego;
};

