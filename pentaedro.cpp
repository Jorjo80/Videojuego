#include "pentaedro.h"
#include <string.h>
#include <stdio.h>
pentaedro::pentaedro(float x, float y,int vidas)
{
	posicion.x=x;
	posicion.y=y;
	vida=vidas;
	//Suelo
	suelo.SetColor(100,100,100);
	suelo.SetPos(0.0f+x,0.0f+y,2.0f,2.0f+x,0.0f+y,-2.0f);
	
	//Pared derecha
	pared_dcha.SetColor(100,150,100);
	pared_dcha.SetPos(2.0f+x,0.0f+y,-2.0f,2.0f+x,2.0f+y,2.0f);
	
	//Pared izquierda
	pared_izq.SetColor(100,150,0);
	pared_izq.SetPos(0.0f+x,0.0f+y,2.0f,2.0f+x,2.0f+y,-2.0f);

	//pared trasera
	trasera.SetColor(100,0,100);
	trasera.SetPos(0.0f+x,0.0f+y,-2.0f,2.0f+x,2.0f+y,-2.0f);

	//delantera
	delantera.SetColor(100,0,0);
	delantera.SetPos(0.0f+x,0.0f+y,2.0f,2.0f+x,2.0f+y,2.0f);

}

pentaedro::pentaedro()
{
	//Suelo
	suelo.SetColor(100,100,100);
	suelo.SetPos(2.0f,1.0f,2.0f,5.0f,1.0f,-2.0f);
	
	//Pared derecha
	pared_dcha.SetColor(100,150,100);
	pared_dcha.SetPos(5.0f,1.0f,-2.0f,5.0f,3.0f,2.0f);
	
	//Pared izquierda
	pared_izq.SetColor(100,150,0);
	pared_izq.SetPos(2.0f,1.0f,2.0f,5.0f,3.0f,-2.0f);

	//pared trasera
	trasera.SetColor(100,0,100);
	trasera.SetPos(2.0f,1.0f,-2.0f,5.0f,3.0f,-2.0f);

	//delantera
	delantera.SetColor(100,0,0);
	delantera.SetPos(2.0f,1.0f,2.0f,5.0f,3.0f,2.0f);
}

pentaedro::~pentaedro(void)
{
}

void pentaedro::Dibuja(void)
{
	suelo.Dibuja(0);
	pared_izq.Dibuja(0);
	pared_dcha.Dibuja(0);
	trasera.Dibuja(1);
	//delantera.Dibuja(1);
	glEnable(GL_TEXTURE_2D);
  
	glBindTexture(GL_TEXTURE_2D, ETSIDI::getTexture("imags/pentaedro.png").id);
	glDisable(GL_LIGHTING);
	glBegin(GL_POLYGON);
	glColor3f(1,1,1);
	glTexCoord2d(1,0);		glVertex3f(2.0f+posicion.x,posicion.y,2); 	
	glTexCoord2d(0,0);		glVertex3f(posicion.x,posicion.y,2);	
	glTexCoord2d(1,1);		glVertex3f(2.0f+posicion.x,2.0f+posicion.y,2);
	glEnd();
	glEnable(GL_LIGHTING);	
	glDisable(GL_TEXTURE_2D);

	_itoa(vida,s,10);
	ETSIDI::setTextColor(1,1,1);
	ETSIDI::setFont("fuentes/BELL.TTF",20);
	ETSIDI::printxy(s,posicion.x+1,(posicion.y+0.5f));
}

int pentaedro::Mueve(void)
{
	posicion.y-=2;
	return (int)posicion.y;
}

