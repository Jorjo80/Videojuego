#include "CoordinadorJuego.h"
#include "glut.h"
#include <iostream>

CoordinadorJuego juego;

//los callback, funciones que seran llamadas automaticamente por la glut
//cuando sucedan eventos
//NO HACE FALTA LLAMARLAS EXPLICITAMENTE
void OnDraw(void); //esta funcion sera llamada para dibujar
void OnTimer(int value); //esta funcion sera llamada cuando transcurra una temporizacion
void OnKeyboardDown(unsigned char key, int x, int y); //cuando se pulse una tecla	


void processMouse(int boton, int estado, int x, int y);

using namespace std;
int main(int argc,char* argv[])
{
	//Inicializar el gestor de ventanas GLUT
	//y crear la ventana
		glutInit(&argc, argv);
		glutInitWindowSize(800,600);
		glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
		glutCreateWindow("ByeByeBlocks");
		glEnable(GL_LIGHT0);
		glEnable(GL_LIGHTING);
		glEnable(GL_DEPTH_TEST);
		glEnable(GL_COLOR_MATERIAL);	
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		glOrtho(-10, 10, 0, 15, -100, 100);
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();


		//habilitar luces y definir perspectiva
	//Registrar los callbacks
		glutDisplayFunc(OnDraw);
		glutTimerFunc(25,OnTimer,0);       //le decimos que dentro de 25ms llame 1 vez a la funcion OnTimer()
		glutKeyboardFunc(OnKeyboardDown);


		glutMouseFunc(processMouse);

		

		
	glutMainLoop();	
}

void OnDraw(void)
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	juego.dibuja();

	//no borrar esta linea ni poner nada despues
	glutSwapBuffers();
}
void OnKeyboardDown(unsigned char key, int x_t, int y_t)
{
	//poner aqui el c�digo de teclado
	juego.tecla(key);

	glutPostRedisplay();
}

void OnTimer(int value)
{
//poner aqui el c�digo de animacion
	juego.mueve();

	juego.mundo3.lanzabola();

	//no borrar estas lineas
	glutTimerFunc(25,OnTimer,0);
	glutPostRedisplay();
}



void processMouse(int button, int state,int x, int y)
{
	
	juego.mundo3.Click(button,state);
}


