#pragma once
#include "esfera.h"
#include "caja.h"
#include "hombre.h"

#include"plant_clase.h"
#include "ETSIDI.h"
#define MAX_ESFERAS 250

class Lista_Esferas
{
public:
	Lista_Esferas(void);
	~Lista_Esferas(void);
	void destruircontenido();
	bool agregar (esfera *e);
	void dibuja();
	void mueve(float t);
	void rebote(caja caja);
	void rebote(pared pared);


	template<class tipo>void reboteobj(tipo x);

	void eliminar(int index);
	void eliminar(esfera *e);
	vector2d GetPos();
	vector2d salida;
	esfera *operator[](int i);
	int getnumero(){return numero;}
	int maximo_total;
	int maximo;
	int num_ant;
	int num_act;
private:
	esfera *lista[MAX_ESFERAS];
	int numero;
	friend class mundo;
};

template <class tipo>void  Lista_Esferas::reboteobj(tipo x)
{
	for(int i=0;i<numero;i++)
	{
		for(int j=0;j<x.numero;j++)
		{
			if(interaccion::rebote(*(lista[i]),*(x.lista[j])))
			{
				--(*(x.lista[j])).vida;
			
			}
		}
	}
}