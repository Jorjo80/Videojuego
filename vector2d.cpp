#include "vector2d.h"
#include "math.h"


vector2d::vector2d(float xv,float yv)
{
	x=xv;
	y=yv;
}


vector2d::~vector2d(void)
{
}

float vector2d::modulo()
{
	return (float)sqrt(x*x+y*y);
}

float vector2d::argumento()
{
	return (float)atan2(y,x);
}

vector2d vector2d::Unitario()
{
	vector2d retorno(x,y);
	float mod=modulo();
	if(mod>0.00001)
	{
		retorno.x/=mod;
		retorno.y/=mod;
	}
	return retorno;
}

vector2d vector2d::operator - (vector2d &v)
{
	vector2d res;
	res.x=x-v.x;
	res.y=y-v.y;
	return res;
}

vector2d vector2d::operator + (vector2d &v)
{
	vector2d res;
	res.x=x+v.x;
	res.y=y+v.y;
	return res;
}

float vector2d::operator *(vector2d &v)
{
	return x*v.x+y*v.y;
}
vector2d vector2d::operator *(float e)
{
	vector2d res;
	res.x=x*e;
	res.y=y*e;
	return res;
}