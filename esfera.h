
#include "vector2d.h"

#pragma once 
class esfera
{
	unsigned char rojo;
	unsigned char verde;
	unsigned char azul;

	float radio;

	vector2d posicion;
	vector2d velocidad;
	vector2d aceleracion;

	 
public:
	~esfera();
	esfera(float rad, float x=0.0f, float y=0.0f, float vx=0.0f, float vy=0.0f);

	void Dibuja(void);
	void Mueve(float t);
	void SetColor( unsigned char r,unsigned char v, unsigned char a);
	void SetRadio(float r);
	void SetPos(float ix,float iy);
	void SetVel(float vx, float vy);
	vector2d GetPos();
	float distancia(esfera e2, vector2d *direccion=0);

	int vida;
	friend class interaccion;
	friend class mundo;

};
