#include "disparo.h"
#include <stdlib.h>
#include "glut.h"
#include "hombre.h"
#include "caja.h"

disparo::disparo(void)
{
	radio=0.25f;
	origen.x=0;
	origen.y=h.altura;
	posicion=origen;
	posicion.y=15.0f;
}


disparo::~disparo(void)
{
}


void disparo::Dibuja(void)
{
	glColor3f(0.0f,1.0f,1.0f);
	glPushMatrix();
	glTranslatef(posicion.x,posicion.y,0);
	glutSolidSphere(radio, 20, 20);
	glPopMatrix();
	glDisable(GL_LIGHTING);
	glColor3f(0.0f,1.0f,1.0f);
	glBegin(GL_LINE_STRIP);
	glVertex3d(origen.x,origen.y,0);
	glVertex3d(posicion.x,posicion.y,0);
	glEnd();
	glEnable(GL_LIGHTING);
}


void disparo::Mueve()
{


	if(posicion.y>=20)
		{
			posicion.y=0;
		}
}

void disparo::Reset(void)
{
	if(posicion.y<15)
		posicion.y=15.0f;
		
}

void disparo::SetPos()
{
	if(posicion.y>15)
			posicion.y=15;

		if(posicion.y<0)
			posicion.y=0;

		if(posicion.x>10)
			posicion.x=10;
		
		if(posicion.x<-10)
			posicion.x=-10;
}
