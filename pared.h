#include "vector2d.h"

#pragma once

class pared
{
	unsigned char rojo;
	unsigned char verde;
	unsigned char azul;

public:
	
	~pared();
	pared();
	
	vector2d limite1;
	vector2d limite2;

	void Dibuja(int id);
	void SetColor( unsigned char r,unsigned char v, unsigned char a);

	float distancia(vector2d punto, vector2d *direccion=0);
	void SetPos(float x1, float y1,float z1, float x2, float y2, float z2);


	friend class Interaccion;
};