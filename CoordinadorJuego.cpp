#include "CoordinadorJuego.h"
#include "glut.h"



CoordinadorJuego::CoordinadorJuego(void)
{
	estado=Inicio;
	ETSIDI::playMusica("sonidos/himno.mp3",TRUE);
	
}


CoordinadorJuego::~CoordinadorJuego(void)
{
}

void CoordinadorJuego::dibuja()
{
	if(estado==Inicio)
	{
			
		ETSIDI::setTextColor(10,0,0);
		ETSIDI::setFont("fuentes/titulo.ttf",30);
		ETSIDI::printxy("BYEBYEBLOCKS GAME",-4,10);
		ETSIDI::setTextColor(1,1,0);
		ETSIDI::setFont("fuentes/nombres.ttf",13);
		ETSIDI::printxy("EDUARDO ARCONES DEL �LAMO & JORGE CONTRERAS ORTIZ",-4.6,6);
		ETSIDI::setTextColor(1,1,1);
		ETSIDI::setFont("fuentes/BELL.TTF",13);
		ETSIDI::printxy("Pulse la tecla - E - para empezar", -5,4);
	
		ETSIDI::printxy("Pulse la tecla - T - para seleccionar personaje", -5,3);
		ETSIDI::printxy("Pulse la tecla -I- para acceder a las instrucciones", -5,2);
		ETSIDI::printxy("Pulse la tecla - S - para salir", -5,1);
		
	}

	if(estado==INSTRUCCIONES)
	{
		ETSIDI::setTextColor(10,0,0);
		ETSIDI::setFont("fuentes/titulo.ttf",30);
		ETSIDI::printxy("INSTRUCCIONES",-4,10);
		ETSIDI::setTextColor(1,1,0);
		ETSIDI::setFont("fuentes/BELL.TTF",13);
		ETSIDI::printxy("Pulse V para volver a la pantalla de inicio",-4.6,6);
		ETSIDI::setTextColor(1,1,1);
		ETSIDI::setFont("fuentes/BELL.TTF",13);
		ETSIDI::printxy("j para mover el disparador a la izquierda", -5,3);
	
		ETSIDI::printxy("k para mover el disparador a la derecha", -5,2);
		ETSIDI::printxy("click izquierdo o barra espaciadora para lanzar bola", -5,1);


	}

	if(estado==Personaje)
	{
		
		ETSIDI::setTextColor(10,0,0);
		ETSIDI::setFont("fuentes/titulo.ttf",30);
		ETSIDI::printxy("BYEBYEBLOCKS GAME",-4,10);
		ETSIDI::setTextColor(1,1,0);
		ETSIDI::setFont("fuentes/BELL.TTF",13);
		ETSIDI::printxy("ELIGE PERSONAJE",-4.6,6);
		ETSIDI::setTextColor(1,1,1);
		ETSIDI::setFont("fuentes/BELL.TTF",13);
		ETSIDI::printxy("1 - Toroide", -5,3);
		ETSIDI::printxy("2 - Esfera", -5,2);
		ETSIDI::printxy("3 - Tetera", -5,1);
	}
	if(estado==GAMEOVER)
	{
		
		ETSIDI::setTextColor(10,0,0);
		ETSIDI::setFont("fuentes/titulo.ttf",30);
		ETSIDI::printxy("GAMEOVER",-4,10);
		ETSIDI::setTextColor(1,1,0);
		ETSIDI::setFont("fuentes/BELL.TTF",13);
		ETSIDI::printxy("Elija la opcion correcta para reiniciar: �quien manda en la capital?",-4.6,6);
		ETSIDI::setTextColor(1,1,1); 
		ETSIDI::setFont("fuentes/BELL.TTF",13);
		ETSIDI::printxy("R - Real Madrid", -5,3);
		ETSIDI::printxy("A - Atletico de Madrid", -5,2);
	}
	if(estado==GOODBYE)
	{
		ETSIDI::setTextColor(10,0,0);
		ETSIDI::setFont("fuentes/titulo.ttf",30);
		ETSIDI::printxy("OPCION INCORRECTA",-4,10);
		ETSIDI::setTextColor(1,1,0);
		ETSIDI::setFont("fuentes/BELL.TTF",13);
		ETSIDI::printxy("HALA MADRID",-4.6,6);
		ETSIDI::setTextColor(1,1,1);
		ETSIDI::printxy("adios", -5,3);
		ETSIDI::setFont("fuentes/nombres.ttf",13);
		ETSIDI::printxy(" created by: Eduardo Arcones del �lamo y Jorge Contreras Ortiz", -5,1);
		glutSwapBuffers();
		int tiempo_ant =glutGet(GLUT_ELAPSED_TIME);
		int tiempo_act;
		int incr_tiempo=0;
		while(incr_tiempo<15000)
		{
			tiempo_act=glutGet(GLUT_ELAPSED_TIME);
			incr_tiempo=tiempo_act-tiempo_ant;
		}
		exit(0);
	}
	
	else if(estado == Juego)
	{
		
		mundo3.Dibuja(tipo_personaje);
		if(mundo3.hexaedros.figuras_fin || mundo3.pentaedros.figuras_fin)
		{
			ETSIDI::stopMusica();
			estado=GAMEOVER;
			mundo3.hexaedros.figuras_fin=mundo3.pentaedros.figuras_fin=0;
			ETSIDI::playMusica("sonidos/gameover.mp3",FALSE);
			
		}
		
	}
}
void CoordinadorJuego::tecla(unsigned char key)
{
	if(estado == Inicio)
	{
		mundo3.Tecla(key);
		tipo_personaje=2; 
		if(key == 'e'||key== 'E')
		{
			ETSIDI::stopMusica();
			ETSIDI::play("sonidos/tecla.mp3");
			mundo3.Inicializa();
			estado=Juego;			
			ETSIDI::playMusica("sonidos/intro.mp3",TRUE);
		}
		if(key == 's'||key== 'S')
			exit(0);

		if(key=='t' || key=='T')
		{
			estado=Personaje;
			ETSIDI::stopMusica();
			ETSIDI::playMusica("sonidos/personaje.mp3",TRUE);
		}
		
		if(key=='i' || key=='I')
		{
			estado=INSTRUCCIONES;
			ETSIDI::stopMusica();
			ETSIDI::playMusica("sonidos/instrucciones.mp3",TRUE);
		}

	
	}

	else if(estado==INSTRUCCIONES)
	{
		mundo3.Tecla(key);
		if(key=='v' || key=='V')
		{
			ETSIDI::play("sonidos/tecla.mp3");
			estado=Inicio;
			ETSIDI::playMusica("sonidos/himno.mp3",TRUE);
		}
	}

	else if(estado==Personaje)
	{
		mundo3.Tecla(key);
		if(key=='1')
		{
			tipo_personaje=1;
			ETSIDI::play("sonidos/tecla.mp3");
			estado=Juego;
			ETSIDI::playMusica("sonidos/intro.mp3",TRUE);
		}
		if(key=='2')
		{
			tipo_personaje=2;
			ETSIDI::play("sonidos/tecla.mp3");
			estado=Juego;
			ETSIDI::playMusica("sonidos/intro.mp3",TRUE);
			
		}
		if(key=='3')
		{
			tipo_personaje=3;
			ETSIDI::play("sonidos/tecla.mp3");
			estado=Juego;
			ETSIDI::playMusica("sonidos/intro.mp3",TRUE);
			
		}
	}

	else if(estado==GAMEOVER)
	{
		mundo3.Tecla(key);
		if(key=='R'  || key=='r')
		{
			ETSIDI::play("sonidos/tecla.mp3");
			estado=Inicio;
			mundo3.esferas.maximo=1;
			for(int i=mundo3.hexaedros.numero-1;i>=0;i--)
			{
				mundo3.hexaedros.lista[i]->vida=0;
				mundo3.hexaedros.eliminar();
			}
			for(int i=mundo3.pentaedros.numero-1;i>=0;i--)
			{
				mundo3.pentaedros.lista[i]->vida=0;
				mundo3.pentaedros.eliminar();
			}
			for(int i=mundo3.Disparos.numero-1;i>=0;i--)
			{
				mundo3.Disparos.lista[i]->vida=0;
				mundo3.Disparos.eliminar();
			}
			for(int i=mundo3.Hor.numero-1;i>=0;i--)
			{
				mundo3.Hor.lista[i]->vida=0;
				mundo3.Hor.eliminar();
			}
			for(int i=mundo3.Ver.numero-1;i>=0;i--)
			{
				mundo3.Ver.lista[i]->vida=0;
				mundo3.Ver.eliminar();
			}
			for(int i=mundo3.Dir.numero-1;i>=0;i--)
			{
				mundo3.Dir.lista[i]->vida=0;
				mundo3.Dir.eliminar();
			}
			mundo3.esferas.num_ant=1;
			
			ETSIDI::stopMusica();
			ETSIDI::playMusica("sonidos/himno.mp3",TRUE);
		}
		if(key=='A'  || key=='a')
		{
			ETSIDI::stopMusica();
			ETSIDI::play("sonidos/himno_real.mp3");
			estado=GOODBYE;
		}
	}
		
	else if(estado==Juego)
	{

		mundo3.Tecla(key);
		
	}
		
}

void CoordinadorJuego::mueve()
{
	if(estado==Juego)
		mundo3.Mueve();
}
