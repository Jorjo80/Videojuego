#include "caja.h"
#include "hombre.h"
#include "esfera.h"
#include "bonus.h"
#include "disparo.h"
#include "hexaedro.h"
#include "pentaedro.h"
#include "Lista_Esferas.h"
#include "bonushorizontal.h"
#include "bonusvertical.h"
#include "bonusdireccion.h"
#include <string.h>
#include <stdio.h>
#include "Plantillabonus.h"
#include "plant_clase.h"


class mundo
{
public:


	Base_fig<hexaedro> hexaedros;
	Base_fig<pentaedro> pentaedros;
	PPTBonus<bonus> Disparos;
	PPTBonus<bonusdireccion> Dir;
	PPTBonus<bonushorizontal> Hor;
	PPTBonus<bonusvertical> Ver;
	disparo disparo;
	hombre hombre;
	caja caja;
	Lista_Esferas esferas;
	char p[10];


	void  bonsVertical(Lista_Esferas e, PPTBonus<bonusvertical> x,Base_fig<hexaedro> h, Base_fig<pentaedro> p);
	void  bonshorizontal(Lista_Esferas e, PPTBonus<bonushorizontal> x, Base_fig<hexaedro> h, Base_fig<pentaedro> p);
	void bonsdirc(Lista_Esferas e, PPTBonus<bonusdireccion> x);
	void bonsDisp(PPTBonus<bonus> x);
	void Tecla(unsigned char key);
	void Click(int button, int state);
	void lanzabola(void);
	void lineabloques(void);
	void Inicializa();
	void Mueve();
	void Dibuja(int a);
	int vida;
	int gatillo;
	int clb;

	friend class CoordinadorJuego;

};
