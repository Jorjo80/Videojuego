#include "esfera.h"
#include"caja.h"
#include <stdlib.h>
#include "glut.h"



esfera::esfera(float rad, float x, float y, float vx, float vy)
{
	vida=1000;
	rojo=verde=azul=255; //blanco
	aceleracion.y=0.0f;
	radio=rad;
	posicion.x=x;
	posicion.y=y;
	velocidad.x=vx;
	velocidad.y=vy;
}


esfera::~esfera(void)
{
}

void esfera::SetColor( unsigned char r,unsigned char v, unsigned char a)
{
	rojo=r;
	verde=v;
	azul=a;
}

void esfera::SetRadio(float r)
{
	if(r<0.1F)
		r=0.1F;
	radio=r;
}

void esfera::SetPos(float ix,float iy)
{
	posicion.x=ix;
	posicion.y=iy;
}

void esfera::Dibuja(void)
{
	glColor3ub(rojo,verde,azul);
	glTranslatef(posicion.x,posicion.y,0);
	glutSolidSphere(radio,20,20);
	glTranslatef(-posicion.x,-posicion.y,0);
}


void esfera::Mueve(float t)
{

	posicion=posicion+velocidad*t+aceleracion*(0.5*t*t);
	velocidad=velocidad+aceleracion*t;


}

void esfera::SetVel(float vx, float vy)
{
	velocidad.x=vx;
	velocidad.y=vy;
}

float esfera::distancia(esfera e2, vector2d *direccion)
{
	vector2d u=posicion-e2.posicion;
	float v=u.modulo()-radio-e2.radio;
	float distancia=v;
	if(direccion!=0) //si nos dan un vector�
	*direccion=u.Unitario();
	return distancia;
}

vector2d esfera::GetPos()
{
	return posicion;
}