#pragma once
#include "basebonus.h"
#include "hexaedro.h"
#include "vector2d.h"
class bonusdireccion :public basebonus
{
public:
	bonusdireccion(float ix, float iy,int vidas);
	~bonusdireccion(void);

	vector2d posicion;

	void Dibuja(void);
	int Mueve();
	friend class interaccion;
};