#include "hombre.h"
#include <stdlib.h>
#include "glut.h"

hombre::hombre(void)
{
	altura=1.8f;
	aceleracion.x=aceleracion.y=0;
	velocidad.x=velocidad.y=0;
	posicion.y=1;
}


hombre::~hombre(void)
{
}


void hombre::Dibuja(int tipo_personaje)
{
	switch (tipo_personaje)
	{
	case 1:
		glPushMatrix();
		glTranslatef(posicion.x,posicion.y,0);
		glColor3f(1.0f,0.0f,0.0f);
		glutSolidTorus( altura/2, altura, 20, 20); 
		glPopMatrix();
		break;
	case 2:
		glPushMatrix();
		glTranslatef(posicion.x,posicion.y,0);
		glColor3f(1.0f,0.0f,0.0f);
		glutSolidSphere(altura, 20, 20);
		glPopMatrix();
		break;
	case 3:
		glPushMatrix();
		glTranslatef(posicion.x,posicion.y,0);
		glColor3f(1.0f,0.0f,0.0f);
		glutSolidTeapot( altura);
		glPopMatrix();
		break;
	default:
		break;
	}
	
}


void hombre::Mueve(float t)
{
	posicion=posicion+velocidad*t+aceleracion*(0.5*t*t);
	velocidad=velocidad+aceleracion*t;

}

void hombre::SetVel( float vx, float vy)
{
	velocidad.x=vx;
	velocidad.y=vy;
}
