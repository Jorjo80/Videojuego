#include "mundo.h"
#include "glut.h"
#include <math.h>
#include "interaccion.h"
#include "ETSIDI.h"


void mundo::Dibuja(int a)
{

	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);	
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(-10, 10, 0, 15, -100, 100);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	esferas.dibuja();
	pentaedros.dibuja();
	hexaedros.dibuja();
	caja.Dibuja();
	hombre.Dibuja(a);
	disparo.Dibuja();
	Disparos.dibuja();
	Ver.dibuja();
	Hor.dibuja();
	Dir.dibuja();
	lineabloques();
	_itoa(esferas.maximo,p,10);
	ETSIDI::setTextColor(1,1,1);
	ETSIDI::setFont("fuentes/BELL.TTF",20);
	ETSIDI::printxy(p,7,2);
}

void mundo::Mueve()
{
	hombre.posicion=esferas.salida;
	disparo.origen=hombre.posicion;
	esferas.mueve(0.025f);
	disparo.Mueve();
	esferas.rebote(caja);
	esferas.reboteobj(esferas);
	esferas.reboteobj(hexaedros);
	esferas.reboteobj(pentaedros);
	bonsVertical(esferas, Ver, hexaedros, pentaedros);
	bonshorizontal(esferas, Hor, hexaedros, pentaedros);
	bonsDisp(Disparos);		
	bonsdirc(esferas,Dir);
	hexaedros.eliminar();
	pentaedros.eliminar(); 
	Disparos.eliminar();
	Ver.eliminar();
	Hor.eliminar();
	Dir.eliminar();
	
} 

void mundo::Inicializa()
{
	vida=0;
}

void mundo::Tecla(unsigned char key)
{

	if((key == 'j')||(key == 'J'))
	{
		
		if(disparo.posicion.y==15)
			disparo.posicion.x--;

		if(disparo.posicion.x==-10)
			disparo.posicion.y--;
		
		if(disparo.posicion.x==10)
			disparo.posicion.y++;

		disparo.SetPos();
		

	}
	if((key == 'k')||(key == 'K'))
	{

		if(disparo.posicion.y==15)
			disparo.posicion.x++;
		
		if(disparo.posicion.x==10)
			disparo.posicion.y--;
	
		if(disparo.posicion.x==-10)
			disparo.posicion.y++;
		
		disparo.SetPos();
	}
	if(key== ' ' && esferas.getnumero()==0)
			gatillo=esferas.maximo;
}


void mundo::Click(int button,int state)
{
	if(button==GLUT_LEFT_BUTTON && esferas.getnumero()==0)
		if(state==GLUT_DOWN )
		{
			gatillo=esferas.maximo;
		}
}

void mundo::lanzabola(void)
{
	clb++;							//clb es el numero de veces que tiene que repetirse los 25 ms entre bolas( es decir retardo del tiempo entre bolas)
	if(gatillo>0 && clb>15 ) 
	{
		clb=0;
		ETSIDI::play("sonidos/disparo.mp3");
		float alpha=atan2((disparo.posicion.y-hombre.altura),(disparo.posicion.x-hombre.posicion.x));
		esferas.agregar(new esfera(0.5f,hombre.posicion.x,hombre.altura,10*cos(alpha),10*sin(alpha)));	
		gatillo--;
	}
}

void mundo::lineabloques(void)
{
	esferas.num_act=esferas.getnumero();
	if(esferas.num_act==0 && esferas.num_ant!=0)
	{
		vida++;
		pentaedros.mueve();
		hexaedros.mueve();
		Disparos.mueve();
		Ver.mueve();
		Hor.mueve();
		Dir.mueve();
		for(int i=0;i<10;i++)
		{
			int tipo=ETSIDI::lanzaDado(6,0);
			float x=(i+1)*2-12;
			switch(tipo)
			{
				case 0:
					Disparos.agregar(new bonus(x+1,14.0f,1));
					break;
				case 1:
					pentaedros.agregar(new pentaedro(x,13.0f,vida));
					break;
				case 2:
					hexaedros.agregar(new hexaedro(x,13.0f,vida));
					break;
				case 3:
					Ver.agregar(new bonusvertical(x+1,14.0f,1));
					break;
				case 4:
					Hor.agregar(new bonushorizontal(x+1,14.0f,1));
					break;
				case 5:
					Dir.agregar(new bonusdireccion(x+1,14.0f,1));
					break;
				default:
					break;
			}
		}
		
	}
	esferas.num_ant=esferas.num_act;
}


void  mundo::bonsVertical(Lista_Esferas e, PPTBonus<bonusvertical> x, Base_fig<hexaedro> h, Base_fig<pentaedro> p)
{
	vector2d w;
	for(int i=0;i<e.numero;i++)
	{
		w=(*(e.lista[i])).velocidad;
		for(int j=0;j<x.numero;j++)
		{
			if(interaccion::rebote(*(e.lista[i]),*(x.lista[j])))
			{				
				for(int f=0;f<h.numero;f++)
				{
					if((*(h.lista[f])).posicion.x==(*(x.lista[j])).posicion.x-1)
						--(*(h.lista[f])).vida;
				}
				for(int f=0;f<p.numero;f++)
				{
					if((*(p.lista[f])).posicion.x==(*(x.lista[j])).posicion.x-1)
						--(*(p.lista[f])).vida;
				}
				--(*(x.lista[j])).vida;
				(*(e.lista[i])).velocidad=w;
			}
			
		}
	}
}
void  mundo::bonshorizontal(Lista_Esferas e, PPTBonus<bonushorizontal> x, Base_fig<hexaedro> h, Base_fig<pentaedro> p)
{
	vector2d w;
	for(int i=0;i<e.numero;i++)
	{
		w=(*(e.lista[i])).velocidad;
		for(int j=0;j<x.numero;j++)
		{
			if(interaccion::rebote(*(e.lista[i]),*(x.lista[j])))
			{	
				for(int f=0;f<h.numero;f++)
				{
					if((*(h.lista[f])).posicion.y==(*(x.lista[j])).posicion.y-1)
						--(*(h.lista[f])).vida;
				}
				for(int f=0;f<p.numero;f++)
				{
					if((*(p.lista[f])).posicion.y==(*(x.lista[j])).posicion.y-1)
						--(*(p.lista[f])).vida;
				}
				--(*(x.lista[j])).vida;
				(*(e.lista[i])).velocidad=w;
			}			
		}		
	}
}


void mundo::bonsdirc(Lista_Esferas e, PPTBonus<bonusdireccion> x)
{
	vector2d w;
	for(int i=0;i<e.numero;i++)
	{
		for(int j=0;j<x.numero;j++)
		{
			if(interaccion::rebote(*(e.lista[i]),*(x.lista[j])))
			{	
				int angulo=ETSIDI::lanzaDado(6,0);
				switch(angulo)
				{
				case 0:
					w.x=10*cos(150);
					w.y=10*sin(150);
					break;
				case 1:
					w.x=10*cos(30);
					w.y=10*sin(30);
					break;
				case 2:
					w.x=10*cos(50);
					w.y=10*sin(50);
					break;
				case 3:
					w.x=10*cos(80);
					w.y=10*sin(80);
					break;
				case 4:
					w.x=10*cos(135);
					w.y=10*sin(135);
					break;
				case 5:
					w.x=10*cos(120);
					w.y=10*sin(120);
					break;
				default:
					break;
				}
				--(*(x.lista[j])).vida;
				(*(e.lista[i])).velocidad=w;
			}			
		}	
	
	}
}
void  mundo::bonsDisp(PPTBonus<bonus> x)
{
	vector2d w;
	
	for(int i=0;i<esferas.numero;i++)
	{
		w=(*(esferas.lista[i])).velocidad;
		for(int j=0;j<x.numero;j++)
		{
			if(interaccion::rebote(*(esferas.lista[i]),*(x.lista[j])))
			{	
				--(*(x.lista[j])).vida;	
				(*(esferas.lista[i])).velocidad=w;							
				if(esferas.maximo<esferas.maximo_total)
				{
					esferas.maximo++;
				}
			}			
		}

	}

}