#pragma once
#include "hexaedro.h"
#include "vector2d.h"
class basebonus:public hexaedro
{
protected:
	float lado;
	vector2d velocidad;
	vector2d aceleracion;
public:
	basebonus(void);
	~basebonus(void);
	virtual void Dibuja(void)=0;
	virtual int Mueve()=0;

};

