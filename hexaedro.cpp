
#include"hexaedro.h"
#include <string.h>
#include <stdio.h>
hexaedro::hexaedro(float x, float y, int vidas)
{
	posicion.x=x;
	posicion.y=y;
	vida=vidas;
	
	//Suelo
	suelo.SetColor(0,100,0);
	suelo.SetPos(0.0f+posicion.x,0.0f+posicion.y,1.0f,2.0f+posicion.x,0.0f+posicion.y,-1.0f);
	
	//Pared derecha
	pared_dcha.SetColor(0,150,0); 
	pared_dcha.SetPos(0.0f+posicion.x,0.0f+posicion.y,1.0f,0.0f+posicion.x,2.0f+posicion.y,-1.0f);
	
	//Pared izquierda
	pared_izq.SetColor(0,150,0);
	pared_izq.SetPos(2.0f+posicion.x,0.0f+posicion.y,1.0f,2.0f+posicion.x,2.0f+posicion.y,-1.0f);

	//Techo
	techo.SetColor(0,100,0);
	techo.SetPos(0.0f+posicion.x,2.0f+posicion.y,1.0f,2.0f+posicion.x,2.0f+posicion.y,-1.0f);

	//fondo//
	fondo.SetColor(0,100,100);
	fondo.SetPos(2.0f+posicion.x,0.0f+posicion.y,-1.0f,0.0f+posicion.x,2.0f+posicion.y,-1.0f);

	//tope//
	tope.SetColor(100,200,100);
	tope.SetPos(2.0f+posicion.x,0.0f+posicion.y,1.0f,0.0f+posicion.x,2.0f+posicion.y,1.0f);

}

hexaedro::hexaedro()
{
	//Suelo
	suelo.SetColor(0,100,0);
	suelo.SetPos(0.0f,0.0f,1.0f,2.0f,0.0f,-1.0f);
	
	//Pared derecha
	pared_dcha.SetColor(0,150,0); 
	pared_dcha.SetPos(0.0f,0.0f,1.0f,0.0f,2.0f,-1.0f);
	
	//Pared izquierda
	pared_izq.SetColor(0,150,0);
	pared_izq.SetPos(2.0f,0.0f,1.0f,2.0f,2.0f,-1.0f);

	//Techo
	techo.SetColor(0,100,0);
	techo.SetPos(0.0f,2.0f,1.0f,2.0f,2.0f,-1.0f);

	//fondo//
	fondo.SetColor(0,100,100);
	fondo.SetPos(2.0f,0.0f,-1.0f,0.0f,2.0f,-1.0f);

	//tope//
	tope.SetColor(100,200,100);
	tope.SetPos(2.0f,0.0f,1.0f,0.0f,2.0f,1.0f);
}

hexaedro::~hexaedro(void)
{
}
void hexaedro::Dibuja(void)
{

	suelo.Dibuja(0);
	techo.Dibuja(0);
	pared_izq.Dibuja(0);
	pared_dcha.Dibuja(0);
	tope.Dibuja(0);
	glEnable(GL_TEXTURE_2D);
  
	glBindTexture(GL_TEXTURE_2D, ETSIDI::getTexture("imags/hexaedro.png").id);
	glDisable(GL_LIGHTING);
	glBegin(GL_POLYGON);
	glColor3f(1,1,1);
	glTexCoord2d(0,1);		glVertex3f(2.0f+posicion.x,posicion.y,2); 
	glTexCoord2d(1,1);		glVertex3f(posicion.x,posicion.y,2);
	glTexCoord2d(1,0);		glVertex3f(posicion.x,2.0f+posicion.y,2);
	glTexCoord2d(0,0);		glVertex3f(2.0f+posicion.x,2.0f+posicion.y,2);
	glEnd();
	glEnable(GL_LIGHTING);	
	glDisable(GL_TEXTURE_2D);
	_itoa(vida,s,10);
	ETSIDI::setTextColor(1,1,1);
	ETSIDI::setFont("fuentes/BELL.TTF",20);
	ETSIDI::printxy(s,posicion.x+1,posicion.y+1);
}

int hexaedro::Mueve(void)
{
	posicion.y=posicion.y-2;

	return (int)posicion.y;
}
