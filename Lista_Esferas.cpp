#include "Lista_Esferas.h"
#include "interaccion.h"


Lista_Esferas::Lista_Esferas(void)
{
	numero=0;
	num_act=0;
	num_ant=1;
	maximo_total=MAX_ESFERAS;
	maximo=1;
	for(int i=0;i<MAX_ESFERAS;i++){
		lista[i]=0;
	}
}

 


Lista_Esferas::~Lista_Esferas(void)
{
}

bool Lista_Esferas::agregar(esfera *e)
{
	if(numero<maximo)	
		lista[numero++]=e;
	else
		return false;
	return true;
}

void Lista_Esferas::dibuja()
{
	for(int i=0;i<numero;i++)
		lista[i]->Dibuja();
}

void Lista_Esferas::mueve(float t)
{
	for (int i=0;i<numero;i++)
		lista[i]->Mueve(t);
}

vector2d Lista_Esferas::GetPos()
{
	for (int i=0;i<numero;i++)
		return lista[i]->GetPos();
}


void Lista_Esferas::rebote(caja caja)
{
	for(int i=0;i<numero;i++){
		interaccion::rebote(*(lista[i]),caja);
	
		if(interaccion::rebote(*(lista[i]),caja.suelo))
		{
			if(getnumero()==maximo)
			{
				salida=lista[i]->GetPos();
			}

			Lista_Esferas::eliminar((lista[i]));
		}
	}
}



void Lista_Esferas::rebote(pared p)
{
	for(int i=0;i<numero;i++)
		interaccion::rebote(*(lista[i]),p);


}


void Lista_Esferas::destruircontenido()
{
	for(int i=0;i<numero;i++)
		delete lista[i];
	numero=0;
}

void Lista_Esferas::eliminar(int index)
{
	if((index<0)||(index>=numero))
		return;
	delete lista[index];
	numero--;
	for(int i=index;i<numero;i++)
		lista[i]=lista[i+1];
}

void Lista_Esferas::eliminar(esfera *e)
{
	for(int i=0;i<numero;i++)
		if (lista[i]==e)
		{
			eliminar(i);
			return;
		}
}


esfera * Lista_Esferas::operator[](int i)
{
	if(i>numero)
		i=numero-1;
	if(i<0)
		i=0;
	return lista[i];
}