#pragma once
class vector2d
{
public: //atributos
	float x;
	float y;
	float z;

public: //m�todos
	vector2d(float xv=0.0f,float yv=0.0f);   // (1)
	virtual ~vector2d();
	float modulo();                          // (2) modulo del vector
	float argumento();                       // (3) argumento del vector
	vector2d Unitario();                     // (4) devuelve un vector unitario
	vector2d operator - (vector2d &);        // (5) resta de vectores
	vector2d operator + (vector2d &);        // (6) suma de vectores
	float operator * (vector2d &);           // (7) producto escalar dos vect.
	vector2d operator * (float);             // (8) producto por un escalar
};