
//IMPLEMENTACION DE LA RELACION DE COMPOSICION
#include "pared.h"	//el include no es necesario que ponga “dominio”
					//porque ya estamos en dicha carpeta.

#pragma once


class caja
{
public:
	caja();

	pared suelo;
	pared techo;
	pared pared_izq;
	pared pared_dcha;
	pared fondo;

	void Dibuja(void);
	friend class interaccion;
};