#pragma once
#include "basebonus.h"
#include "vector2d.h"
#include "hexaedro.h"
class bonusvertical :public basebonus
{
public:
	bonusvertical(float ix, float iy, int vidas);
	~bonusvertical(void);
	vector2d posicion;

	void Dibuja(void);
	int Mueve();
	friend class interaccion;
};
