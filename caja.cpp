#include "caja.h"
#include "ETSIDI.h"
#include "glut.h"


caja::caja(void)
{
	//Suelo
	suelo.SetColor(50,100,0);
	suelo.SetPos(-10.0f,0,10.0f,10.0f,0,-10.0f);
	
	//Pared derecha
	pared_dcha.SetColor(100,150,0);
	pared_dcha.SetPos(-10.0f,0,10.0f,-10.0f,15.0f,-10.0f);
	
	//Pared izquierda
	pared_izq.SetColor(100,150,0);
	pared_izq.SetPos(10.0f,0,10.0f,10.0f,15.0f,-10.0f);

	//Techo
	techo.SetColor(50,100,0);
	techo.SetPos(-10.0f,15.0f,10.0f,10.0f,15.0f,-10.0f);

	//fondo//
	fondo.SetColor(50,100,50);
	fondo.SetPos(10.0f,0,-10.0f,-10.0f,15.0f,-10.0f);

}


/*caja::~caja(void)
{
}
*/

void caja::Dibuja(void)
{
	suelo.Dibuja(0);
	techo.Dibuja(0);
	pared_izq.Dibuja(0);
	pared_dcha.Dibuja(0);
	//fondo.Dibuja(3);
	glEnable(GL_TEXTURE_2D);
  
	glBindTexture(GL_TEXTURE_2D, ETSIDI::getTexture("imags/Fondo.png").id);
	glDisable(GL_LIGHTING);
	glBegin(GL_POLYGON);
	glColor3f(1,1,1);
	
	glTexCoord2d(0,1);		glVertex3f(-10,0,-0.1);
	glTexCoord2d(1,1);		glVertex3f(10,0,-0.1);
	glTexCoord2d(1,0);		glVertex3f(10,15,-0.1);
	glTexCoord2d(0,0);		glVertex3f(-10,15,-0.1);
	glEnd();

	glEnable(GL_LIGHTING);	
	glDisable(GL_TEXTURE_2D);
}