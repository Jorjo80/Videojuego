#include "bonusdireccion.h"
#include <stdlib.h>
#include "glut.h"


bonusdireccion::bonusdireccion(float x, float y,int vidas)
{
	posicion.x=x;
	posicion.y=y;
	vida=vidas;
	
	//Suelo
	suelo.SetColor(0,100,0);
	suelo.SetPos(0.0f+posicion.x,0.0f+posicion.y,1.0f,0.25f+posicion.x,0.0f+posicion.y,-1.0f);
	
	//Pared derecha
	pared_dcha.SetColor(0,150,0); 
	pared_dcha.SetPos(0.0f+posicion.x,0.0f+posicion.y,1.0f,0.0f+posicion.x,0.25f+posicion.y,-1.0f);
	
	//Pared izquierda
	pared_izq.SetColor(0,150,0);
	pared_izq.SetPos(0.25f+posicion.x,0.0f+posicion.y,1.0f,0.25f+posicion.x,0.25f+posicion.y,-1.0f);

	//Techo
	techo.SetColor(0,100,0);
	techo.SetPos(0.0f+posicion.x,0.25f+posicion.y,1.0f,1.0f+posicion.x,0.25f+posicion.y,-1.0f);

	//fondo//
	fondo.SetColor(0,100,100);
	fondo.SetPos(0.25f+posicion.x,0.0f+posicion.y,-1.0f,0.0f+posicion.x,0.25f+posicion.y,-1.0f);

	//tope//
	tope.SetColor(0,200,200);
	tope.SetPos(0.25f+posicion.x,0.0f+posicion.y,1.0f,0.0f+posicion.x,0.25f+posicion.y,1.0f);
}


bonusdireccion::~bonusdireccion(void)
{
}


void bonusdireccion::Dibuja(void)
{
	suelo.Dibuja(0);
	techo.Dibuja(0);
	pared_izq.Dibuja(0);
	pared_dcha.Dibuja(0);
	tope.Dibuja(0);
}


int bonusdireccion::Mueve()
{
	posicion.y=posicion.y-2;
	return (int)posicion.y;
}
