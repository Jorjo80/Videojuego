#include "hombre.h"
#include "caja.h"
#include "esfera.h"
#include "pared.h"
#include "hexaedro.h"
#include"pentaedro.h"
#include "disparo.h"
#include "Lista_Esferas.h"
#include "bonus.h"
#include "bonusdireccion.h"
#include "bonushorizontal.h"
#include "bonusvertical.h"

#pragma once

class interaccion
{
public:
	
	interaccion(void);
	virtual ~interaccion(void);
	
	static bool bons(esfera &,bonus &);
	static bool bons(esfera &,bonushorizontal &);
	static bool bons(esfera &,bonusvertical &);
	static bool bons(esfera &,bonusdireccion &);

	static bool rebote(esfera &e, pared p);
	static void rebote(esfera &e, caja c);

	static bool rebote(esfera &e, hexaedro &h);
	static bool rebote(esfera &e, pentaedro &p);

	static bool rebote(esfera &e1, esfera &e2);

	static bool colision(esfera e, hombre h);

};

