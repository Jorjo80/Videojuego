#include "bonus.h"
#include <stdlib.h>
#include "glut.h"
#include "ETSIDI.h"


bonus::bonus(float x, float y,int vidas)
{
	lado=0.5f;
	posicion.x=x;
	posicion.y=y;
	vida=vidas;
	
	
	//Suelo
	suelo.SetColor(0,100,0);
	suelo.SetPos(0.0f+posicion.x,0.0f+posicion.y,1.0f,lado+posicion.x,0.0f+posicion.y,-1.0f);
	
	//Pared derecha
	pared_dcha.SetColor(0,150,0); 
	pared_dcha.SetPos(0.0f+posicion.x,0.0f+posicion.y,1.0f,0.0f+posicion.x,lado+posicion.y,-1.0f);
	
	//Pared izquierda
	pared_izq.SetColor(0,150,0);
	pared_izq.SetPos(lado+posicion.x,0.0f+posicion.y,1.0f,lado+posicion.x,lado+posicion.y,-1.0f);

	//Techo
	techo.SetColor(0,100,0);
	techo.SetPos(0.0f+posicion.x,lado+posicion.y,1.0f,lado+posicion.x,lado+posicion.y,-1.0f);

	//fondo//
	fondo.SetColor(0,100,100);
	fondo.SetPos(lado+posicion.x,0.0f+posicion.y,-1.0f,0.0f+posicion.x,lado+posicion.y,-1.0f);

	//tope//
	tope.SetColor(0,0,0);
	tope.SetPos(lado+posicion.x,0.0f+posicion.y,1.0f,0.0f+posicion.x,lado+posicion.y,1.0f);

	
}


bonus::~bonus(void)
{
}


void bonus::Dibuja(void)
{
	suelo.Dibuja(0);
	techo.Dibuja(0);
	pared_izq.Dibuja(0);
	pared_dcha.Dibuja(0);
	tope.Dibuja(0);
	glEnable(GL_TEXTURE_2D);
  
	glBindTexture(GL_TEXTURE_2D, ETSIDI::getTexture("imags/disparo.png").id);
	glDisable(GL_LIGHTING);
	glBegin(GL_POLYGON);
	glColor3f(1,1,1);
	glTexCoord2d(0,1);		glVertex3f(lado+posicion.x,posicion.y,2); 
	glTexCoord2d(1,1);		glVertex3f(posicion.x,posicion.y,2);
	glTexCoord2d(1,0);		glVertex3f(posicion.x,lado+posicion.y,2);
	glTexCoord2d(0,0);		glVertex3f(lado+posicion.x,lado+posicion.y,2);
	glEnd();
	glEnable(GL_LIGHTING);	
	glDisable(GL_TEXTURE_2D);
}


int bonus::Mueve()
{
	posicion.y=posicion.y-2;
	return (int)posicion.y;
}
